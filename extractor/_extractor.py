import logging

import requests

from bs4 import BeautifulSoup
from fixtures import SELECTORS, EXTRACTORS


class ExtractorBase:

    def __read(self, url, safe=True):
        try:
            html_page = requests.get(url, timeout=10)
        except Exception as e:
            if safe:
                return self.__read(url, safe=False)
            logging.warning("Exception: {}\t{}".format(url, str(e)))
            return
        else:
            self.status_code = html_page.status_code
            self.soup = BeautifulSoup(html_page.text, 'html.parser')

    def read_path(self, path=None):
        if path is not None:
            url = self.site.url + path
        elif self.path is not None:
            url = self.path
        else:
            url = self.site.url
        logging.info('Reading: {}'.format(url))
        self.__read(url)

    class Meta:
        abstract = True


class LinkExtractor(ExtractorBase):
    def __init__(self, site, path=None):
        self.site = site
        self.path = path
        self.soup = None
        self.successful = False
        self.status_code = 0
        self.site_selector = self.__get_site_selector()

    def __get_site_selector(self):
        selectors = SELECTORS.get(self.site.url, None)
        if selectors is None:
            return None
        return selectors.get('site', None)

    def extract_all(self):
        links = set()
        if self.soup is None:
            return links
        if self.site_selector is None:
            target = self.soup
        else:
            target = self.soup.select(self.site_selector)

        for link in target.findAll('a'):
            links.add(link.get('href'))
        logging.info('Total links: {}'.format(len(links)))
        return links

    def extract_filtered(self, attrs):
        links = set()
        if self.soup is None:
            return links
        for link in self.soup.findAll('a', attrs=attrs):
            links.add(link.get('href'))
        logging.debug('Total links: {}'.format(len(links)))
        return links

    def get_links(self, path=None, attrs=None):
        self.read_path(path)
        if attrs is None:
            return self.extract_all()
        return self.extract_filtered(attrs)


class ArticleExtractor(ExtractorBase):
    def __init__(self, site, path=None):
        self.site = site
        self.path = path
        self.cat_selector = SELECTORS[self.site.url].get('category')
        self.title_selector = SELECTORS[self.site.url].get('title')
        self.body_selector = SELECTORS[self.site.url].get('body')
        self.cat_extractor = EXTRACTORS[self.site.url].get('category')
        self.title_extractor = EXTRACTORS[self.site.url].get('title')
        self.body_extractor = EXTRACTORS[self.site.url].get('body')
        self.soup = None
        self.elements = None
        self.data = None
        self.status_code = 0

    @property
    def successful(self):
        if self.status_code // 100 != 2:
            return False
        return True

    def find_elements(self):
        self.elements = dict()
        if self.cat_selector is not None:
            self.elements['category'] = self.soup.select(self.cat_selector)
        self.elements['title'] = self.soup.select(self.title_selector)
        self.elements['body'] = self.soup.select(self.body_selector)

    def get_data(self):
        self.data = {}
        if self.elements is not None:
            if 'category' in self.elements.keys() and self.cat_extractor is not None:
                self.data['category'] = self.cat_extractor(self.elements['category'][0])
            else:
                self.data['category'] = None
            if 'title' in self.elements.keys() and len(self.elements['title']):
                self.data['title'] = self.title_extractor(self.elements['title'][0])
            else:
                self.data['title'] = None

            if 'body' in self.elements.keys() and len(self.elements['body']):
                self.data['body'] = self.body_extractor(self.elements['body'][0])
            else:
                self.data['body'] = None
            # for element in ['category', 'title', 'body']:
            #     if element in self.elements.keys() and len(self.elements[element]):
            #         self.data[element] = self.

    def get_article(self):
        self.read_path()
        if not self.successful:
            return None
        self.find_elements()
        self.get_data()
        return self.data
