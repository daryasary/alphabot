from fixtures.mappers import *
from utils import clear_body_skip_div, clear_body, get_category, strip_title, mixin, get_link, mapper

SITES = (
    'https://www.ddizim.org',
    "https://www.ensonhaber.com",
    "https://www.trthaber.com",
    'https://www.haberler.com',
    'https://www.mynet.com'
)

CATEGORIES = {
    'https://www.example.com': ['/fa/cookery', '/fa/celebrity', '/fa/health'],
    'https://www.ensonhaber.com': [],
    "https://www.ddizim.org": [],
    "https://www.trthaber.com": ['/haber/gundem', '/haber/turkiye', '/haber/dunya', '/haber/ekonomi', '/haber/saglik', '/haber/kultur-sanat', '/haber/bilim-teknoloji', '/haber/yasam'],
    'https://www.haberler.com': ['/ekonomi', '/spor', '/magazin', '/dunya'],
    'https://www.mynet.com': ['/magazin/', '/trend/ilginc-haberler', '/magazin/kultur-sanat', '/kadin/kadin-tv', '/kadin/', '/guncel-haberler', '/spor/diger-sporlar', '/egitim-haberler', '/yemek/k/yemek-tv', '/yasam-haberler', '/magazin-videolari-izle', '/kadin/kendin-yap', '/haber', '/politika-haberler', '/magazin/guncel', '/spor-videolari-izle', '/secim-haberler', '/kadin/burclar-astroloji', '/spor/canli-mac-anlatimi-ve-sonuclari', '/yemek/k/yapsana', '/dizi-videolari-izle', '/yemek/yemek-tarifleri', '/yemek/k/kesfet', '/trend/video', '/spor/futbol', '/kadin/altin-kulup', '/trend/alisveris', '/yemek/', '/trend/espor', '/spor/basketbol', '/dunya-haberler', '/son-dakika-haberleri', '/trend/eglence', '/trend/kanatlandiran-haberler', '/kadin/annecocuk', '/video', '/trend/yasam', '/kadin/guzellik-moda', '/trend', '/trend/teknoloji', '/magazin/foto-analiz', '/spor/', '/magazin/dizi', '/haber-videolari-izle', '/yasamsaglik-videolari-izle', '/yerel-haberler', '/spor/iddaa/puandurumu?ligid=1', '/ardahan', '/sinop', '/manisa', '/gumushane', '/mugla', '/canakkale', '/tokat', '/osmaniye', '/igdir', '/kocaeli', '/adana', '/ordu', '/bartin', '/sanliurfa', '/kirklareli', '/yozgat', '/tekirdag', '/tunceli', '/giresun', '/amasya', '/usak', '/bursa', '/denizli', '/karaman', '/bolu', '/elazig', '/erzincan', '/izmir', '/istanbul', '/nevsehir', '/yalova', '/antalya', '/yerel-haberler', '/kars', '/isparta', '/bilecik', '/aksaray', '/kilis', '/siirt', '/afyonkarahisar', '/balikesir', '/sirnak', '/kayseri', '/burdur', '/hakkari', '/mus', '/zonguldak', '/bingol', '/batman', '/konya', '/ankara', '/erzurum', '/gaziantep', '/kahramanmaras', '/nigde', '/trabzon', '/cankiri', '/edirne', '/aydin', '/malatya', '/karabuk', '/adiyaman', '/sakarya', '/kutahya', '/bayburt', '/mardin', '/mersin', '/rize', '/artvin', '/agri', '/bitlis', '/hatay', '/kastamonu', '/kirsehir', '/sivas', '/corum', '/kirikkale', '/diyarbakir', '/van', '/duzce', '/eskisehir', '/samsun']

}

LINK_REGEX = {
    "https://www.ensonhaber.com": '^/[a-zA-Z0-9]+-[a-z0-9A-Z]+-',
    "https://www.ddizim.org": '/izle/',
    "https://www.trthaber.com": 'haber/[a-zA-Z0-9]+/[a-zA-Z0-9]+-[a-z0-9A-Z]+',
    'https://www.haberler.com': '/[a-zA-Z0-9]+-[a-z0-9A-Z]+',
    'https://www.mynet.com': '/[a-zA-Z0-9]+-[a-z0-9A-Z]+',
}

PAGINATION_REGEX = {
    "https://www.ddizim.org": dict(regex='/l.php?sayfa={}', start_from=1),
    "https://www.ensonhaber.com": dict(regex='/son-dakika/{}', start_from=1),
    "https://www.trthaber.com": dict(regex='/{}.sayfa.html', start_from=1),
    "https://www.haberler.com": dict(regex='/s{}/', start_from=1, limit=10),
}

SELECTORS = {
    'https://www.example.com': dict(
        category=".news_path a:nth-child(3)", body=".body", title="h1.title  a:nth-child(1)", video='#video_html5_api',
        site="section.box:nth-child(2)"
    ),
    'https://www.ddizim.org': dict(
        site="section.box:nth-child(2)", title=".title > a:nth-child(1)", body=".item-body", category=None
    ),
    "https://www.ensonhaber.com": dict(
        category=None, body='.content > article:nth-child(1)', site=".ui-list", title='.articleTitle > h1:nth-child(1)'
    ),
    "https://www.trthaber.com": dict(category=None, body='.detYazi', site=".ui-list", title='#trth11'),
    "https://www.haberler.com": dict(category=None, body='.haber_metni', site=".ui-list", title='#haber_baslik'),
    "https://www.mynet.com": dict(category=None, body='#contextual', site=".ui-list", title='body > div.container.container-detail > div > div.row.content-head > div > h1'),
}

EXTRACTORS = {
    'https://www.example.com': dict(category=mapper(mixin(get_category, get_link), EXAMPLE_SITE_CAT_MAPPER), body=clear_body, title=strip_title),
    'https://www.example.com/tag/bio': dict(category=lambda x: 2, body=clear_body_skip_div, title=strip_title),
    "https://www.ensonhaber.com": dict(category=None, body=clear_body_skip_div, title=strip_title),
    "https://www.trthaber.com": dict(category=None, body=clear_body, title=strip_title),
    "https://www.haberler.com": dict(category=None, body=clear_body, title=strip_title),
    "https://www.mynet.com": dict(category=None, body=clear_body, title=strip_title),
}
