from threading import Thread
from queue import Queue

from core._extract_articles import update_article_from_queue, put_articles
from core._extract_links import put_links, extract_link_from_queue
from lib.decorators import log_time
from local_settings import ARTICLES_THREAD_COUNT, LINKS_THREAD_COUNT


@log_time
def extract_articles_multi_thread(site):
    queue = Queue()
    article_process = list()
    put_articles(queue, site)
    for i in range(ARTICLES_THREAD_COUNT):
        name = 'Thread #{}'.format(i + 1)
        process = Thread(target=update_article_from_queue, args=(name, queue), name=name)
        process.start()
        article_process.append(process)
    queue.join()

    for process in article_process:
        process.join()


@log_time
def extract_links_multi_thread(site):
    queue = Queue()
    links_process = list()
    put_links(queue, site)
    for i in range(LINKS_THREAD_COUNT):
        name = 'Thread #{}'.format(i + 1)
        process = Thread(target=extract_link_from_queue, args=(name, queue, site), name=name)
        process.setName('Thread #{}'.format(i + 1))
        process.start()
        links_process.append(process)

    queue.join()

    for process in links_process:
        process.join(timeout=1)
