import logging

from extractor import ArticleExtractor
from models import Article, Site


def extract_article(article, site):
    extractor = ArticleExtractor(site=site, path=article.url)
    data = extractor.get_article()
    # logging.info('##### {}\t{}\t{}'.format(extractor.successful, data.get('title', None) if extractor.successful else '', article.id))
    if extractor.successful and bool(data.get('title', None)):
        article.title = data['title']
        article.body = data['body']
        if data['category'] is not None:
            article.category = data['category']
        article.status = 5
    else:
        article.status = 3
    article.save()
    return extractor


def update_article_from_queue(name, queue):
    while not queue.empty():
        article = queue.get()
        logging.info('[{}] Queue size: {}\tArticle: {}'.format(name, queue.qsize(), article.id))
        site = Site.get(Site.id == 2)
        article.status = 1
        article.save()
        extract_article(article, site)
        queue.task_done()
    logging.info('[{}] All tasks done'.format(name))


def update_article_by_id(aid):
    article = Article.get(id=aid)
    site = Site.get(Site.id == article.site_id)
    article.status = 1
    article.save()
    return extract_article(article, site)


def put_articles(queue, site=None):
    if site is None:
        articles = Article.select().where(Article.status == 0)
    else:
        articles = Article.select().where(Article.status == 0, Article.site_id == site.id)
    [queue.put(article) for article in articles]


def extract_articles(site=None):
    if site is None:
        articles = Article.select().where(Article.status == 0)
    else:
        articles = Article.select().where(Article.status == 0, Article.site_id == site.id)
    succeed = 0
    for article in articles:
        res = update_article_by_id(article.id)
        succeed = succeed + 1 if res else succeed
    logging.info("Total:{}\tSucceed:{}\tFailed:{}".format(articles.count(), succeed, articles.count() - succeed))
