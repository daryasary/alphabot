import logging
import re

from sentry_sdk import capture_message

from extractor import LinkExtractor
from fixtures import CATEGORIES, LINK_REGEX, PAGINATION_REGEX
from lib.decorators import log_time
from models import Article
from utils import paginator, check_link


def save_links(links, site, cat=None, queue=None):
    if cat is None:
        extra_kwargs = {'site_id': site.id}
    else:
        extra_kwargs = {'site_id': site.id, 'category': cat}
    total = len(links)
    new = 0
    while len(links):
        link = links.pop()
        article, created = Article.store(check_link(site.url, link), **extra_kwargs)
        if created:
            new += 1
        if queue is not None:
            queue.put(article)
        logging.debug("count: {}\tcategory: {}\tarticle: {}\tlink: {}".format(len(links), cat, article.id, link))
    rate = new / total if new != 0 else 0
    logging.info("Total:{}\tNew:{}\tExists:{}\tRate:{}".format(total, new, total - new, rate))
    return rate


def get_links(extractor, cat=None):
    link_regex = LINK_REGEX.get(extractor.site.url, None)
    paginating_regex = PAGINATION_REGEX.get(extractor.site.url, None)

    page = paginator(**paginating_regex) if paginating_regex is not None else None
    base_path = cat if cat is not None else ''
    attrs = {'href': re.compile(link_regex)} if link_regex is not None else None

    is_available = True
    while is_available:
        path = base_path + page.__next__() if page is not None else base_path
        links = extractor.get_links(path, attrs)
        is_available = bool(extractor.status_code == 200) and len(links) and bool(page)
        yield cat, links


def get_category_links_generator(extractor, categories):
    for cat in categories:
        yield get_links(extractor, cat)


def extract_from_categories(extractor, categories, queue=None):
    for cat_links_generator in get_category_links_generator(extractor, categories):
        for cat_link in cat_links_generator:
            cat, links = cat_link
            rate = save_links(links, extractor.site)
            if rate < 0.1:
                break


def extract_from_site(extractor, queue=None):
    for links in get_links(extractor):
        rate = save_links(links[1], extractor.site)
        if rate < 0.1:
            break


def put_links(queue, site):
    categories = CATEGORIES.get(site.url)
    [queue.put(category) for category in categories]


def extract_link_from_queue(name, queue, site):
    extractor = LinkExtractor(site=site)
    while not queue.empty():
        category = queue.get()
        logging.info('[{}] Queue size: {}\tcategory: {}'.format(name, queue.qsize(), category))
        try:
            for links in get_links(extractor, cat=category):
                rate = save_links(links[1], extractor.site)
                if rate < 0.1:
                    break
        except Exception as e:
            capture_message(str(e))
            pass
        queue.task_done()
    logging.info('[{}] All tasks done'.format(name))


@log_time
def extract_links(site, queue=None):
    extractor = LinkExtractor(site=site)
    logging.info("Extracting links from {}".format(site.url))
    categories = CATEGORIES.get(site.url)
    if len(categories) == 0:
        extract_from_site(extractor, queue)
    else:
        extract_from_categories(extractor, categories, queue)
