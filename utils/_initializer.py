from models import Article
from models._base import mysql_db


def create_tables():
    with mysql_db:
        mysql_db.create_tables([Article])


# create_tables()