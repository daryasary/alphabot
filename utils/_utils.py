import json
from datetime import datetime

from bs4 import NavigableString

get_category = lambda s: s.split('/')[2] if len(s.split('/')) > 2 else None


def check_link(site, s):
    if site in s:
        return s
    if site.endswith('/') and s.startswith('/'):
        return site + s[1:]
    elif (site.endswith('/') and s.startswith('/')) or (site.endswith('/') or s.startswith('/')):
        return site + s
    return site + '/' + s


def get_link(cat):
    try:
        result = cat.get('href')
    except:
        return cat
    return result


def store_file(data, file_format='json'):
    filename = 'files/links-{}.{}'.format(datetime.now().strftime('%y-%m-%d-%H-%M'), file_format)
    if file_format == 'json':
        data = json.dumps(data)
    with open(filename, 'w') as f:
        f.write(data)


def clear_body_skip_div(body):
    items = []
    for item in body:
        if isinstance(item, NavigableString):
            continue
        images = item.find_all('img')
        videos = item.find_all('iframe')
        if not images and not videos and item.name not in ['script', 'style', 'div']:
            items.append(item.get_text())
        elif images:
            for img in images:
                items.append("<img src={}/>".format(img.get('src')))
        elif videos:
            for video in videos:
                items.append('<video id="player_html5_api" preload="auto" src="{}"></video>'.format(video.get('src')))
    return ' '.join(items)


def clear_body(body):
    items = []
    for item in body:
        if isinstance(item, NavigableString):
            continue
        images = item.find_all('img')
        videos = item.find_all('iframe')
        if item.name not in ['script', 'style']:
            try:
                items.append(item.get_text())
            except:
                pass
        if images:
            for img in images:
                items.append("<img src={}/>".format(img.get('src')))
        elif videos:
            for video in videos:
                items.append('<video id="player_html5_api" preload="auto" src="{}"></video>'.format(video.get('src')))
    return ' '.join(items)


def strip_title(title):
    try:
        text = title.strip()
    except:
        text = title.text.strip()
    return text


def paginator(regex, start_from, limit=0):
    while True:
        yield regex.format(start_from)
        start_from += 1
        if limit and start_from > limit:
            return None


def mixin(f1, f2):
    """
    Provide ability to mix two function and when user call them execute one by one
    """

    def mix(x):
        return f1(f2(x))

    return mix


def mapper(func, map_dict):
    def map_value(x):
        default = map_dict.get('default', None)
        return map_dict.get(func(x), default)

    return map_value
