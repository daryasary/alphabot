from peewee import *
from datetime import datetime

from local_settings import MYSQL, POSTGRES


mysql_db = MySQLDatabase(
    MYSQL['NAME'], user=MYSQL['USER'], password=MYSQL['PASSWORD'], host=MYSQL['HOST'], port=MYSQL['PORT']
)

pg_db = PostgresqlDatabase(
    POSTGRES['NAME'], user=POSTGRES['USER'], password=POSTGRES['PASSWORD'], host=POSTGRES['HOST'], port=POSTGRES['PORT']
)


class BaseModel(Model):
    """A base model that will use our MySQL database"""
    created_time = DateTimeField(default=datetime.now())
    modified_time = DateTimeField(default=datetime.now())

    class Meta:
        database = pg_db
