from peewee import *

from models._base import BaseModel


class Site(BaseModel):
    url = CharField(unique=True)
    is_enable = BooleanField(default=True)
