from peewee import *

from models._base import BaseModel
from models.utils import get_or_create


class Article(BaseModel):
    title = CharField(null=True)
    body = TextField(null=True)
    url = CharField(unique=True)

    status = IntegerField(default=0)
    site_id = IntegerField(null=True)
    category = CharField(null=True)

    @classmethod
    def store(cls, url, **kwargs):
        article, created = get_or_create(cls, url=url, defaults=kwargs)
        return article, created
