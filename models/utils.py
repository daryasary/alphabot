def get_or_create(model, **kwargs):
    defaults = kwargs.pop('defaults')
    try:
        instance = model.get(**kwargs)
        created = False
    except model.DoesNotExist:
        instance = model.create(**{**kwargs, **defaults})
        created = True
    return instance, created
