import logging
from datetime import datetime


def log_time(func):
    """
    Decorator to store start_time and end_time of the process and calc duration
    :param func:
    :return:
    """
    def runner(*args, **kwargs):
        start_time = datetime.now()
        response = func(*args, **kwargs)
        end_time = datetime.now()
        logging.info('[Time logger] Duration:{}\tStart:{}\tEnd:{}'.format(end_time - start_time, start_time, end_time))
        return response
    return runner
