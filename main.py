import argparse
import logging

import sentry_sdk

from core import extract_links, extract_articles, check_site, extract_articles_multi_thread, extract_links_multi_thread
from local_settings import SENTRY_SDK_CONFIG
from models import Site


def parse_arguments():
    """
    python main.py extract https://www.ddizim.org/ --limit 100

    python main.py read https://www.ddizim.org/

    python main.py update https://www.ddizim.org/
    
    python main.py update https://www.ddizim.org/ --log-file filename
    """
    parser = argparse.ArgumentParser(description='Run user\'s time detection main process')
    parser.add_argument('handler', help='Main purpose handler')
    parser.add_argument('site', help='Destination site')
    parser.add_argument('--log_type', choices=['file', 'console'], help='Select which log type you prefer, file or console? (Default is console)', default='console')
    parser.add_argument('--limit', type=int, help='Define limit for number of articles on site (Default is unlimited)')

    args = parser.parse_args()

    if args.log_type == 'file':
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', filename='main.log', level=logging.INFO)
    else:
        logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

    handlers_map = dict(
        extract=extract_links, read=extract_articles, update=check_site, multi_read=extract_articles_multi_thread,
        multi_extract=extract_links_multi_thread,
    )

    sentry_sdk.init(SENTRY_SDK_CONFIG)

    try:
        handler = handlers_map[args.handler]
        logging.info('Handler {} received'.format(args.handler))
    except Exception as e:
        logging.error("No proper handler found for input")
        return
    if args.site:
        try:
            site = Site.get(Site.url == args.site)
        except Site.DoesNotExist:
            logging.error("Site does not exists")
            return
        handler(site=site)
    logging.info('{} ended successfully.'.format(args.handler))


if __name__ == "__main__":
    parse_arguments()
