from fixtures import *

"""
After adding a new site and before restarting crawler should 
check below steps, this code will do this authomatically:

    1. check related categories for all sites exists in CATEGORIES list and is not empty
    2. Check for all css selectors for all sites
    3. Check for all extractor values and existance of all defined functions
"""


def check():
    for site in SITES:
        assert site in CATEGORIES.keys(), "site {} categories does not added to the CATEGORIES".format(site)
        # assert len(CATEGORIES[site]) > 0, "site {} categories is empty".format(site)
        # assert "default" in EXTRACTORS[site]["category"].keys(), "site {} has not default category".format(site)
        assert site in SELECTORS.keys(), "site {} selectors are not added to the SELECTORS".format(site)
        # assert all([(s in SELECTORS[site].keys()) and (SELECTORS[site][s] not in [None, '']) for s in ['title', 'body', 'category']]), "site {} selectors are not completely added".format(site)
        assert site in EXTRACTORS.keys(), "site {} extractors are not added to the EXTRACTORS"
        # assert all([(s in EXTRACTORS[site].keys()) and (EXTRACTORS[site][s] not in [None, '']) for s in ['title', 'body', 'category']]), "site {} selectors are not completely added".format(site)


if __name__ == '__main__':
    check()
